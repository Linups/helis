<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    protected $table = 'feed';
    
    protected $guarded = ['id'];
    
    public function category() {
        return $this->belongsTo(Category::class, 'category_id');
    }
    
    public function content() {
        return $this->hasMany(Content::class, 'id'); // feed_id
    }
}

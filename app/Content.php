<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'content';
    
    protected $guarded = ['id'];
    
    public function feed() {
        return $this->belongsTo(Feed::class, 'feed_id');
    }
}

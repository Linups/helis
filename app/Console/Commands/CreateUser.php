<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Validator;
use App\User;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates an admin user.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //--- Getting input
        $input['name'] = $this->ask('Please enter your name.');
        $input['email'] = $this->ask('Please enter your email.');
        $input['password'] = $this->secret('Please enter your password.');
        $input['password_confirm'] = $this->secret('Please repeat your password.');
        //--- Validating input        
        $validator = Validator::make($input, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'password_confirm' => 'required|same:password'
            
        ]);
        
        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->all() as $message) {
                $this->error($message);
            }
        } else {
            $user = new User();
            $user->name = $input['name'];
            $user->email = $input['email'];
            $user->password = bcrypt($input['password']);
            $user->save();
            
            $this->info('User successfully created.');
        }
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Feed;
use App\Plugins\Curl;
use App\Content;

class FeedUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates feeds.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Curl $curl)
    {
        $feeds = Feed::all();
        if(count($feeds)>0) {
            $i = 0;
            foreach($feeds as $feed) {
                $feedData = $curl->get($feed->url);
                $xmlData = simplexml_load_string($feedData);                
                
                if(count($xmlData->channel->item)>0){
                    foreach($xmlData->channel->item as $xml) {
                        $i++;
                        $contentArray = array('title' => $xml->title, 'link' => $xml->link);
                        
                        if(isset($xml->description)) $contentArray['description'] = $xml->description;
                        if(isset($xml->category)) $contentArray['category'] = $xml->category;
                        
                        $contentArray['feed_id'] = $feed->id;
                        
                        Content::firstOrCreate($contentArray);
                    }
                }
                
            }
        }
        $this->info('Imported '.$i.' '.str_plural('feed', $i));
        
    }
}

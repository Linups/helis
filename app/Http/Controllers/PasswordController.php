<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;
use App\Http\Requests\PasswordRequest;

class PasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function edit() {
        return view('auth.passwords.update');
    }
    
    public function update(PasswordRequest $request) {
        //--- Validating old password
        if (Hash::check($request->current_password, Auth::user()->password)) {
            //--- Updating password
            $user = User::find(Auth::id());
            $user->password = \Hash::make($request->new_password);
            $user->save();
            
            $request->session()->flash('message', 'Pasword successfully updated');
        } else {
            return back()->withErrors('Your current password is wrong.');
        }
        
        return Redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feed;
use App\Category;
use Session;
use Redirect;
use App\Http\Requests\FeedRequest;

class FeedController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index() {
        $data = Feed::get();
        
        return view('feed.index', compact('data'));
    }
    
    public function create() {
        $data = Category::get();
        
        return view('feed.create', compact('data'));
    }
    
    public function store(FeedRequest $request){
        
        $feed = new feed();
        $feed->title = $request->title;
        $feed->url = $request->url;
        $feed->category_id = $request->category;
        $feed->save();
        
        $request->session()->flash('message', 'Feed url successfully added!');
        
        return Redirect()->back();
    }
    
    public function show(\App\Feed $feed) {  
        return view('feed.show', compact('feed'));
    }
    
    public function edit(\App\Feed $feed) {
        $feed->category = Category::get();
        return view('feed.edit', compact('feed'));
    }
    
    public function update(\App\Feed $feed, FeedRequest $request) {
        $feed->title = $request->title;
        $feed->url = $request->url;
        $feed->category_id = $request->category;
        $feed->save();
        
        $request->session()->flash('message', 'Feed url successfully updated!');
        
        return Redirect()->back();
    }
    
    public function destroy(\App\Feed $feed) {
        $feed->delete();

        Session::flash('message', 'Successfully deleted!');
        return Redirect::to('feeds');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Content;
use App\Category;

class FrontPageController extends Controller
{
    public function index(Request $request) {
        if(isset($request->category)) {
            $content = Content::select('content.title AS title', 'content.description AS description', 'feed.title AS feed_title', 'content.link AS feed_url')
                    ->leftJoin('feed', 'content.feed_id', '=', 'feed.id')
                    ->leftJoin('category', 'feed.category_id', '=', 'category.id')
                    ->where('category.id', '=', $request->category)
                    ->orderBy('content.created_at', 'DESC')
                    ->paginate(20);
        } else {
            $content = Content::select('content.title AS title', 'content.description AS description', 'feed.title AS feed_title', 'content.link AS feed_url')
                    ->leftJoin('feed', 'content.feed_id', '=', 'feed.id')
                    ->orderBy('content.created_at', 'DESC')
                    ->paginate(20);
        }
        
        $categories = Category::all();
        
        return view('welcome', compact('content', 'categories'));
    }
    
}

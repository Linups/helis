<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Http\Requests\CategoryRequest;


class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index() {
        $data = Category::get();
        return view('category', compact('data'));
    }
    
    public function store(CategoryRequest $request) {
        
        Category::create($request->all());
        
        return Redirect()->back();
    }
}

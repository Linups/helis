<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    protected $fillable = ['title'];
    
    public function feed() {
        return $this->hasMany(Feed::class, 'id');
    }
}

<?php

namespace App\Plugins;

class Curl {
    public function post($target, $data, $option = false) {
        $ch = curl_init();
        if (!isset($option['header'])) {
            $post_vars = '';
            foreach ($data as $key => $value) {
                $post_vars .= $key . "=" . $value . "&";
            }
        } else {
            $post_vars = $data;
        }
        curl_setopt($ch, CURLOPT_URL, $target);
        curl_setopt($ch, CURLOPT_POST, 1);                //0 for a get request
        curl_setopt($ch, CURLOPT_POSTFIELDS,$post_vars);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        
        if (isset($option['header']) && $option['header'] == 'json') {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data)));
        }
        
        
        $response = curl_exec($ch);
        
        curl_close ($ch);
        
        return $response;
    }
    
    public function get($target) {
        $cp = curl_init($target);
        curl_setopt($cp, CURLOPT_TIMEOUT, 12000);
        curl_setopt($cp, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($cp, CURLOPT_FOLLOWLOCATION, 1);
        $data = curl_exec($cp);
        curl_close($cp);
        return $data;
    }
}

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Update your password:</div>

                <div class="panel-body">
                    <form action="/update-password" method="post">
                        {{ csrf_field() }}
                        <input name="_method" type="hidden" value="PUT">
                        <div class="form-group">
                            <label for="current_password">Current password:</label>
                            <input type="password" name="current_password" id="current_password" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="new_password">New password:</label>
                            <input type="password" name="new_password" id="new_password" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="repeat_new_password">Repeat new password:</label>
                            <input type="password" name="repeat_new_password" id="repeat_new_password" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="update-password" class="btn btn-primary center-block">Update password</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
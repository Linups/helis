@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Manage categories</div>

                <div class="panel-body">
                    <form action="/categories" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="title">Category title</label>
                            <input type="text" name="title" value="{{ old('title', '') }}" class="form-control" required id="title" />
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                    
                    @if(isset($data) && count($data)>0)
                    <table class="table table-bordered table-striped">
                        <tr><th>Nr.</th><th>Title</th></tr>
                        @foreach($data as $category)
                            <tr><td>{{ $loop->iteration }}</td><td>{{ $category->title }}</td></tr>
                        @endforeach
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

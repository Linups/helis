@if (Session::has('message'))
        <div class="alert alert-info center-block text-center">{{ Session::get('message') }}</div>
@endif
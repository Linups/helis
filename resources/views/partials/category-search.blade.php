<div class="panel-heading custom-heading">
    <form action="/" method="post">
        {{ csrf_field() }}
        <div class="col-sm-4"><div class="text-center"><h4>Select category:</h4></div></div>
        <div class="col-sm-4">
            <div class="form-group">
                <select name="category" class="form-control">
                    @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->title }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <button type="submit" class="btn btn-primary center-block">Filter</button>
            </div>
        </div>
    </form>
</div>
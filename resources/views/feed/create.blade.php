@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add new feed</div>

                <div class="panel-body">
                    <form action="/feeds" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="title">Title:</label>
                            <input type="text" name="title" id="title" value="{{ old('title', '') }}" class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label for="url">Link:</label>
                            <input type="text" name="url" id="url" value="{{ old('url', '') }}" class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label for="category">Category</label>
                            <select name="category" id="category" class="form-control" required>
                                @foreach($data as $category)
                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary center-block">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

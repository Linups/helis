@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Update feed</div>

                <div class="panel-body">
                    <form action="/feeds/{{ $feed->id }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label for="title">Title:</label>
                            <input type="text" name="title" id="title" value="{{ $feed->title }}" class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label for="url">Link:</label>
                            <input type="text" name="url" id="url" value="{{ $feed->url }}" class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label for="category">Category</label>
                            <select name="category" id="category" class="form-control" required>
                                @foreach($feed->category as $category)
                                    @if($category->id == $feed->category_id)
                                        <option value="{{ $category->id }}" selected="selected">{{ $category->title }}</option>
                                    @else
                                        <option value="{{ $category->id }}">{{ $category->title }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary center-block">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Feed: <div class="pull-right"><a href="/feeds"><button class="btn btn-info">Return Back</button></a></div>
                <div class="panel-body">
                        <div class="form-group">
                            <label for="title">Title:</label>
                            <p>{{ $feed->title }}</p>
                        </div>
                        <div class="form-group">
                            <label for="url">Link:</label>
                            <p>{{ $feed->url }}</p>
                        </div>
                        <div class="form-group">
                            <label for="category">Category</label>
                            <p>{{ $feed->category->title }}</p>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Manage Feeds</div>

                <div class="panel-body">
                    <div class="form-group">
                        <a href="/feeds/create">Add new feed</a>
                    </div>                    
                    @if(isset($data) && count($data) > 0)
                    <table class="table table-bordered table-striped">
                        <tr><th>Nr.</th><th>Title</th><th>Link</th><th>Show</th><th>Edit</th><th>Delete</th></tr>
                        @foreach ($data as $feed)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $feed->title }}</td>
                            <td>{{ $feed->url }}</td>
                            <td><a href="/feeds/{{ $feed->id }}"><button class="btn btn-success">Show</button></a></td>
                            <td><a href="/feeds/{{ $feed->id }}/edit"><button class="btn btn-warning">Edit</button></a></td>
                            <td><form action="/feeds/{{ $feed->id }}" method="post" onsubmit="return confirm('Warning. All associated content will be deleted.')">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button class="btn btn-danger">Delete</button></form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

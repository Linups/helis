@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                @include('partials.category-search')
                <div class="panel-body">
                    @if(isset($content) && count($content)>0)
                    @foreach($content as $post)
                    <article>
                        <h4>
                            <a href="#" data-toggle="modal" data-target="#myModal" class="link" title="{{ $post->feed_url }}">{{ $post->title }}</a> 
                            
                            ({{ $post->feed_title }})
                        </h4>
                        <div class="body">{!! $post->description or "" !!}</div>
                    </article>
                    <hr>
                    @endforeach
                    {{ $content->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@include('partials.modal-window')
@endsection
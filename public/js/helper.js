$(document).ready(function(){
    console.log('started');
    $(".link").click(function() {
        var title = $(this).text();
        console.log(title);
        var url = $(this).attr('title');
        console.log(url);
        var body = $(this).closest('article').find('.body').text()
        console.log(body);
        //--- Setting values
        $('#myModalLabel').html(title);
        $('#myModalBody').html(body);
        $("#myLink").attr("href", url);
    });
});
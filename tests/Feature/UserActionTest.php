<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Category;
use App\Feed;
use App\User;

class UserActionTest extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * A basic test example.
     *
     * @return void
     */

    public function testUserCanChangePassword() { 
        $password = 'currentPassword';
        $newPassword = 'newPassword';
        
        $user = $this->createUser(['name' => 'TestName', 'password' => \Hash::make($password)]);

        $this->get('/update-password')->assertStatus(200)->assertViewIs('auth.passwords.update');
        
        $response = $this->call('PUT', '/update-password', array(
            '_token' => csrf_token(),
            'current_password' => $password,
            'new_password' => $newPassword,
            'repeat_new_password' => $newPassword,
        ));
        
        $response->assertStatus(302);
        
        $updatedUser = User::find($user->id);
        
        $this->assertTrue(\Hash::check($newPassword, $updatedUser->password));                        
    }

    
    public function testUserCanCreateFeedUrl() {
        $this->createUser();
        
        $this->get('/feeds/create')->assertStatus(200)->assertSee('Add new feed');
        
        $category = Category::inRandomOrder()->first();
        
        $response = $this->call('POST', '/feeds', array(
            '_token' => csrf_token(),
            'title' => 'Feed Title',
            'url' => 'https://www.alfa.lt/rss',
            'category' => $category->id,
        ));
        
        $response->assertStatus(302);
        $this->assertDatabaseHas('feed', ['title' => 'Feed Title', 'url' => 'https://www.alfa.lt/rss', 'category_id' => $category->id]);
    }
    
    public function testUserCanFailCreateFeedUrl() {
        $this->createUser();
        
        $this->get('/feeds/create')->assertStatus(200)->assertSee('Add new feed');
        
        $category = Category::inRandomOrder()->first();
        
        $response = $this->call('POST', '/feeds', array(
            '_token' => csrf_token(),
            'title' => 'F',
            'url' => 'https://www.alfa.lt/rss',
            'category' => $category->id,
        ));
        $response->assertStatus(302);
        
        $this->assertDatabaseMissing('feed', ['title' => 'F', 'url' => 'https://www.alfa.lt/rss', 'category_id' => $category->id]);
        
        $response->assertSessionHasErrors(['title']);
    }
    
    public function testUserCanEditFeedUrl() {
        $this->createUser();
        
        $category = Category::inRandomOrder()->first();
        $feed = new Feed();
        $feed->title = 'Feed Title2';
        $feed->url = 'https://www.alfa.lt/rss2';
        $feed->category_id = $category->id;
        $feed->save();
        
        $this->get('/feeds/'.$feed->id.'/edit')->assertStatus(200)->assertSee('Update feed');
        
        $response = $this->call('POST', '/feeds', array(
            '_token' => csrf_token(),
            'title' => 'updated Feed Title2',
            'url' => 'https://www.alfa.lt/rss/updated',
            'category' => $category->id,
        ));
        $response->assertStatus(302);
        $this->assertDatabaseHas('feed', ['title' => 'updated Feed Title2', 'url' => 'https://www.alfa.lt/rss/updated', 'category_id' => $category->id]);
        
    }
    
    public function testUserCanFailEditFeedUrl() {
        $this->createUser();
        
        $category = Category::inRandomOrder()->first();
        $feed = new Feed();
        $feed->title = 'Feed Title2';
        $feed->url = 'https://www.alfa.lt/rss2';
        $feed->category_id = $category->id;
        $feed->save();
        
        $this->get('/feeds/'.$feed->id.'/edit')->assertStatus(200)->assertSee('Update feed');
        
        $response = $this->call('POST', '/feeds', array(
            '_token' => csrf_token(),
            'title' => 'u',
            'url' => 'https://www.alfa.lt/rss/updated',
            'category' => $category->id,
        ));
        $response->assertStatus(302);
        
        $this->assertDatabaseMissing('feed', ['title' => 'u', 'url' => 'https://www.alfa.lt/rss/updated', 'category_id' => $category->id]);
        
        $response->assertSessionHasErrors(['title']);
        
    }
    
    public function testUserCanDeleteFeedUrl() {
        $this->createUser();
    
        $category = Category::inRandomOrder()->first();
        $feed = new Feed();
        $feed->title = 'Feed Title3';
        $feed->url = 'https://www.alfa.lt/rss3';
        $feed->category_id = $category->id;
        $feed->save();
        
        $this->assertDatabaseHas('feed', ['id' => $feed->id]);
  
        
        $response = $this->call('DELETE', '/feeds/'.$feed->id, array(
            '_token' => csrf_token()
        ));
        $response->assertStatus(302);
        
        $this->assertDatabaseMissing('feed', ['id' => $feed->id]);
    }
    
    public function testUserCanCreateCategory() {
        $this->createUser();
        
        $this->get('/categories')->assertStatus(200);
        
        $response = $this->call('POST', '/categories', array(
            '_token' => csrf_token(),
            'title' => 'Category Title',
        ));
        $response->assertStatus(302);
        
        $this->assertDatabaseHas('category', ['title' => 'Category Title']);
    }
    
    public function testUserCanFailCreatingCategory() {
        $this->createUser();
        
        $this->get('/categories')->assertStatus(200);
        
        $response = $this->call('POST', '/categories', array(
            '_token' => csrf_token(),
            'title' => '11',
        ));
        $response->assertStatus(302);
        
        $this->assertDatabaseMissing('category', ['title' => '111']);
        
        $response->assertSessionHasErrors(['title']);
        
    }
    
    
    
    public function createUser($param = NULL) {
        if($param === NULL) $param = array();
        $user = factory(\App\User::class)->create($param);
        $this->actingAs($user);
        return $user;
    }
}




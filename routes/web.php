<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontPageController@index');
Route::post('/', 'FrontPageController@index');

Auth::routes();
Route::get('/update-password', 'PasswordController@edit');
Route::put('/update-password', 'PasswordController@update');
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('/categories', 'CategoryController@index');
Route::post('/categories', 'CategoryController@store');

Route::resource('feeds', 'FeedController');
/*
Route::get('/feeds', 'FeedController@index');
Route::get('/feeds/create', 'FeedController@create');
Route::post('/feeds', 'FeedController@store');
Route::get('/feeds/{feed}/edit', 'FeedController@edit');
//Route::get('/feeds/{feed}', 'FeedController@show');
Route::get('/feeds/{feed}', function (App\Feed $feed) {
    return view('feed.show', compact('feed'));
});
Route::put('/feeds/{feed}', 'FeedController@update');
Route::delete('/feeds/{feed}', 'FeedController@destroy');
*/
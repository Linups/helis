<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 1024);
            $table->text('description')->nullable();
            $table->string('category')->nullable();
            $table->string('link', 1024);
            $table->unsignedInteger('feed_id')->index();
            $table->timestamps();
            
            $table->foreign('feed_id')->references('id')->on('feed')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content');
    }
}

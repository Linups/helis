<?php

use Illuminate\Database\Seeder;
use App\Category;
use Carbon\Carbon;

class FeedTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {


        DB::table('feed')->insert([
            'title' => 'Alfa.lt',
            'url' => 'http://feeds.feedburner.com/technologijos-visos-publikacijos?format=xml',
            'category_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('feed')->insert([
            'title' => 'Technologijos.lt',
            'url' => 'http://feeds.feedburner.com/technologijos-visos-publikacijos?format=xml',
            'category_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('feed')->insert([
            'title' => 'Delfi.lt Dienos Naujienos',
            'url' => 'https://www.delfi.lt/rss/feeds/daily.xml',
            'category_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('feed')->insert([
            'title' => 'Delfi.lt Politiko akimis',
            'url' => 'https://www.delfi.lt/rss/feeds/politics.xml',
            'category_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }

}

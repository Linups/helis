<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->insert([
            'title' => 'Dienos naujienos'
        ]);
        
        DB::table('category')->insert([
            'title' => 'Technologijos'
        ]);
        
        DB::table('category')->insert([
            'title' => 'Politika'
        ]);
    }
}

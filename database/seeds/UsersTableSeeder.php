<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Linas',
            'email' => 'linas.gutauskas@gmail.com',
            'password' => bcrypt('linas'),
        ]);
    }
}
